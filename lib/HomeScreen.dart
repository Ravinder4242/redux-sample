import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider_for_redux/provider_for_redux.dart';
import 'package:reduxsample/main.dart';
import 'package:reduxsample/post.dart';
import 'package:reduxsample/redux/actions/LoginApiActions.dart';
import 'package:reduxsample/redux/state.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    fetchPosts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ReduxConsumer<AppState>(
      builder: (ctx, store, state, dispatch, child) => Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(),
        drawer: Drawer(),
        body: renderPosts(state)));
  }

  Widget renderPosts(AppState state){
    if(state.postItemsState.isLoading){
      return Center(child: CircularProgressIndicator(),);
    }else if(state.postItemsState.isError){
      return Center(child: Text("Something went wrong"),);
    }
    else{
      return RefreshIndicator(
        child: ListView.builder(
          itemBuilder: (context, int) {
            return Card(
              color: Colors.white,
              child: ListTile(
                title: Text(
                  state.postItemsState.posts[int].title,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
                subtitle: Text(
                  state.postItemsState.posts[int].body,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      color: Colors.black),
                ),
              ),
            );
          },
          itemCount: state.postItemsState.posts.length,
        ),
        onRefresh: fetchPosts,
      );
    }
  }

  Future<Null> fetchPosts() async {
    List<Post> posts = [];
    store.dispatch(LoginApiActions([], true, false));
    final response =
        await http.get("https://jsonplaceholder.typicode.com/posts");
    if (response.statusCode == 200) {
      store.dispatch(LoginApiActions([], false, false));
      posts = postFromJson(response.body);
    } else if(response.statusCode == 400 || response.statusCode == 403 || response.statusCode == 500){
      store.dispatch(LoginApiActions([], false, true));
    }else {
      posts = [];
    }
    store.dispatch(LoginApiActions(posts, false, false));
  }
}
