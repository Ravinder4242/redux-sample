import 'package:flutter/material.dart';
import 'package:reduxsample/post.dart';

class PostModel {
  bool isLoading = false;
  bool isError = false;
  List<Post> posts = [];

  PostModel(
      {@required this.isLoading, @required this.isError, @required this.posts});
}
