import 'dart:async';

import 'package:async_redux/async_redux.dart';
import 'package:reduxsample/model/postModel.dart';
import 'package:reduxsample/post.dart';
import 'package:reduxsample/redux/state.dart';

class LoginApiActions extends ReduxAction<AppState>{
  List<Post> posts;
  bool isLoading;
  bool isError;


  LoginApiActions(this.posts, this.isLoading, this.isError);

  @override
  Future<AppState> reduce() async{
    if(isLoading){
      return state.copyWith(postModel: new PostModel(isLoading: isLoading, isError: false, posts: []));
    }else if(isError){
      return state.copyWith(postModel: new PostModel(isLoading: false, isError: isError, posts: []));
    }else if(posts != null && posts.length > 0){
      return state.copyWith(postModel: new PostModel(isLoading: false, isError: false, posts: posts));
    }
    return null;
  }

}