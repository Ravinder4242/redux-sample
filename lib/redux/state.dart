import 'package:reduxsample/model/postModel.dart';

class AppState {
  PostModel postItemsState;

  AppState({this.postItemsState});

  AppState copyWith({PostModel postModel}) {
    return AppState(postItemsState: postModel ?? this.postItemsState);
  }

  static AppState initialState() => AppState(
      postItemsState:
          new PostModel(isLoading: false, isError: false, posts: []));
}
